import dataclasses
from dataclasses import dataclass
import psycopg2
import yaml
from debian.debian_support import Version

import list_updates

@dataclass
class SecuityIssueRelease:
    source: str
    issue: str
    release: str
    fixed_version: Version
    status: str
    urgency: str

with open("tails-amd64-stable@f8dce1140e-20230426T0716Z.build-manifest", "rt") as f:
    build_manifest = yaml.safe_load(f)

source_pkgs = list_updates.get_source_pkgs(build_manifest)

conn = psycopg2.connect("postgresql://udd-mirror:udd-mirror@udd-mirror.debian.net/udd")
cursor = conn.cursor()
cursor.execute("SELECT source, issue, release, fixed_version, status, urgency from security_issues_releases where source in %s and release = 'bullseye'", (tuple(source_pkgs.keys()),))
cves =(SecuityIssueRelease(*i) for i in cursor.fetchall())
open_cves = list()
for cve in cves:
    if cve.status == "resolved":
        if source_pkgs[cve.source] < cve.fixed_version:
            print(f"{cve.source} ({cve.issue}) {source_pkgs[cve.source]} vs {cve.fixed_version} -> Update available, that fix CVE.")
    else:
        open_cves.append(cve)

with open("open_cves.yaml","wt") as f:
    l = list(dataclasses.asdict(i) for i in open_cves)
    yaml.dump(l,f)
